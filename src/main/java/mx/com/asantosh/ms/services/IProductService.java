package mx.com.asantosh.ms.services;

import java.util.List;

import mx.com.asantosh.ms.beans.product.ProductBean;

public interface IProductService {

	public List<ProductBean> findAll();
	
	public ProductBean finById(Long id);
	
	
}
