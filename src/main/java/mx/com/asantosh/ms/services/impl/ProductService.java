package mx.com.asantosh.ms.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.asantosh.ms.beans.client.product.ProductClientBean;
import mx.com.asantosh.ms.beans.client.product.dao.ProductDao;
import mx.com.asantosh.ms.beans.product.ProductBean;
import mx.com.asantosh.ms.helpers.IProductHelper;
import mx.com.asantosh.ms.services.IProductService;

@Service
public class ProductService implements IProductService{

	@Autowired
	private ProductDao productoDao;
	
	@Autowired
	private IProductHelper productHelper;
	
	@Override
	@Transactional(readOnly = true)
	public List<ProductBean> findAll() {
		
		return  productHelper.toProductList((List<ProductClientBean>)productoDao.findAll());
	}

	@Override
	@Transactional(readOnly = true)
	public ProductBean finById(Long id) {
		
		return productHelper.toProduct(productoDao.findById(id).orElse(new ProductClientBean()));
	}

}
