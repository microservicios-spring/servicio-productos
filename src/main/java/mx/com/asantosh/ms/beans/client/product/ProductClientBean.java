package mx.com.asantosh.ms.beans.client.product;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "products")
public class ProductClientBean implements Serializable{
	
	private static final long serialVersionUID = 7314815289329197079L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private Long id;
	private String name;
	private Double price;
	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	

}
