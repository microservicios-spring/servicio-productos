package mx.com.asantosh.ms.beans.client.product.dao;

import org.springframework.data.repository.CrudRepository;

import mx.com.asantosh.ms.beans.client.product.ProductClientBean;

public interface ProductDao extends CrudRepository<ProductClientBean, Long> {

}
