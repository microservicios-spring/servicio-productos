package mx.com.asantosh.ms.beans.product;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class ProductBean implements Serializable{

	private static final long serialVersionUID = 3699126150017564221L;
	
	private Long id;
	private String name;
	private Double price;
	private Date createAt;

}
