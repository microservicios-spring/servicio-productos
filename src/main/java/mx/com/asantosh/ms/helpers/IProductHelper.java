package mx.com.asantosh.ms.helpers;

import java.util.List;

import mx.com.asantosh.ms.beans.client.product.ProductClientBean;
import mx.com.asantosh.ms.beans.product.ProductBean;

public interface IProductHelper {
	
	public ProductBean toProduct(ProductClientBean product);
	
	public List<ProductBean> toProductList(List<ProductClientBean> products);
}
