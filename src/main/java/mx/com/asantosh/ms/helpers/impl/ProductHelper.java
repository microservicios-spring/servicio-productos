package mx.com.asantosh.ms.helpers.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import mx.com.asantosh.ms.beans.client.product.ProductClientBean;
import mx.com.asantosh.ms.beans.product.ProductBean;
import mx.com.asantosh.ms.helpers.IProductHelper;

@Component
public class ProductHelper implements IProductHelper {

	@Override
	public ProductBean toProduct(ProductClientBean productClientBean) {
		ProductBean productBean = new ProductBean();
		productBean.setId(productClientBean.getId());
		productBean.setName(productClientBean.getName());
		productBean.setPrice(productClientBean.getPrice());
		productBean.setCreateAt(productClientBean.getCreateAt());
		return productBean;
	}

	@Override
	public List<ProductBean> toProductList(List<ProductClientBean> products) {
		if(products != null && !products.isEmpty()) {
			products.stream().map(this::toProduct).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}

	

	


	
	
	
	

}
